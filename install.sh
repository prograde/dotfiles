home="/home/$1"
if [ ! $home ]; then
  echo "directory $home not found" >&2
  exit 1
fi

source=$(dirname $(realpath $0))

if [ -d $home/.vim ] || [ -h $home/.vim ]; then
  mv $home/.vim $home/.vim-old
fi
ln -s $source/.vim $home/.vim

if [ -f $home/.vimrc ] || [ -h $home/.vimrc ]; then
  mv $home/.vimrc $home/.vimrc-old
fi
ln -s $source/.vim/.vimrc $home/.vimrc

if [ -f $home/.bashrc ] || [ -h $home/.bashrc ]; then
  mv $home/.bashrc $home/.bashrc-old
fi
ln -s $source/.bashrc-debian $home/.bashrc

if [ -f $home/.gitconfig ] || [ -h $home/.gitconfig ]; then
  mv $home/.gitconfig $home/.gitconfig-old
fi
ln -s $source/.gitconfig $home/.gitconfig


sudo cp $source/usr_share_X11_xkb_symbols_us_sv /usr/share/X11/xkb/symbols/us_sv
if [ -f $home/.xinitrc ] || [ -h $home/.vim ]; then
  mv $home/.xinitrc $home/.xinitrc-old
fi
ln -s $source/.xinitrc $home/.xinitrc

