" install vim-plug if not installed
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" start loading plugins
call plug#begin('~/.vim/plugged')

" Plug 'tpope/vim-sensible'                         " agreeable vim settings
Plug 'tpope/vim-surround'                           " surround with parenthesis and such
" Plug 'junegunn/fzf'                               " fuzzy find
" Plug 'junegunn/fzf.vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}     " intellisense
" Plug 'vim-airline/vim-airline'
" Plug 'editorconfig/editorconfig-vim'
" Plug 'rakr/vim-one'
" Plug 'https://github.com/rbtnn/vim-game_engine'
" Plug 'https://github.com/rbtnn/vim-mario'

"" `PlugInstall` to install new plugins after editing this list
call plug#end()

" set coc to let <cr> confirm code completion
" see: https://github.com/neoclide/coc.nvim/wiki/Completion-with-sources#use-tab-or-custom-key-for-trigger-completion
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm() : "\<C-g>u\<CR>"

" load CtrlP-plugin
set runtimepath^=~/.vim/bundle/ctrlp.vim

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

" show some nice colors
colorscheme elflord
syntax on

" show line numbers
set number
set relativenumber

" set tabs and indents
set expandtab
set tabstop=2
set shiftwidth=2

" show current file
set laststatus=2

" skip swap files
set noswapfile

" more natural splits
set splitbelow
set splitright
